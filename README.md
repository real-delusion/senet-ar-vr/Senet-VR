# SENET VR

_Project for the Advanced Interactive Environments Project_

_VR experience in Unity for Senet's game_

- **Team:**

    Real Delusion AR/VR

- **Team members:**
  - Aarón Cabrera Mir
  - Alejandro Mira Abad
  - Joan Ciprià Moreno Teodoro
  - Emilia Rosa van der Heide
  - Santiago Perez Torres
  
- **Platform:** 

    Unity
